<?php 

function get_json_contents(string $url) {
    for ($retry = 1; $retry <= 3; ++$retry) {
        $data = file_get_contents($url);

        if ($data !== false) {
            return json_decode($data, true);
        }

        echo("Error downloading file ($url) retry#$retry<br/>\n\n");
        sleep(15);
    }

    die();
    // return null;
}

function floor_decimal($num, $precision = 8) {
    $integerPart = floor($num);
    $decimalPart = str_replace($integerPart, '', $num);
    $trimmedDecimal = substr($decimalPart, 0, $precision + 1);
    return $integerPart . $trimmedDecimal;
}

function floorp($val, $precision)
{
    $mult = pow(10, $precision);      
    return floor($val * $mult) / $mult;
}

function addMCNSignal($action, $symbol, $price, $quantity, $sym2, $quote){

    global $balances, $core_account_value, $lot_size, $full_lots, $tradeableAssets, $memo, $newtrades, $api;

	$action == 'S' ? 'SELL' : 'BUY';
    $msg = '';
    $noFunds = 0;

    // UPDATE BNB
    try {
        sleep(rand(2,5));
        if($action == 'B'){
    
                if($core_account_value >= $lot_size){
                    $lot_qty = $quantity * $lot_size;
                    $buynow = 1;
                    
                    foreach ($balances as $key => $balance) {
                        if($key == $sym2 && $sym2 != 'BTC' && $sym2 != 'BUSD' && $sym2 != 'ETH' && $sym2 != 'USDT'){
                            if($balance["btc_value"] >= $full_lots){
                                $buynow = 0;
                            } else if(($balance["btc_value"] + $lot_qty) >= $full_lots){
                                $lot_qty = $full_lots - $balance["btc_value"];
                            }
                        }
                    }

                    // IF QUOTE IS NOT BITCOIN FIND VALUE IN QUOTE PRICE
                    // if($quote == 'USDT' || $quote == 'BUSD'){
                    //     $lot_qty = $lot_qty * $balances['BTC']["btc_price"];
                    // }if($quote == 'ETH'){
                    //     $lot_qty = $lot_qty / $balances['ETH']["btc_price"];
                    // }

                    if($lot_qty > $balances[$quote]["coin_qty"]){
                        $lot_qty = $balances[$quote]["coin_qty"]*0.995;
                    }

                    if($lot_qty > $balances[$quote]["coin_qty"]){
                        $noFunds = 100;
                        $msg .= '<tr class="table-warning"><td colspan="5" class="black">BUY '.$lot_qty.' '.$symbol.' - insufficient funds avaiable only '.$balances[$quote]["coin_qty"].' '.$quote.' available.</td></tr>';
                    } else {

                        $minNotional = $tradeableAssets[$symbol]['minNotional'];
                        if($lot_qty < $filter['minNotional']){
                            if($core_account_value >= $filter['minNotional']){
                                $lot_qty = $filter['minNotional'];
                            } else {
                                $noFunds = 100;
                                $msg .= '<tr class="table-warning"><td colspan="5" class="black">BUY '.$symbol.' - insufficient funds to meet minimum ('.$minNotional.' '.$quote.') trade requirement.</td></tr>';
                            }
                        }
                        // foreach ($tradeableAssets['symbols'] as $symbols) {
                        //     if($symbols['symbol'] == $symbol){
                        //         foreach ($symbols['filters'] as $filter) {
                                        
                        //             if($filter['filterType'] == 'MIN_NOTIONAL'){
                        //                 $minNotional = $filter['minNotional'];
                        //                 if($lot_qty < $filter['minNotional']){
                        //                     if($core_account_value >= $filter['minNotional']){
                        //                         $lot_qty = $filter['minNotional'];
                        //                     } else {
                        //                         $noFunds = 100;
                        //                         $msg = '<tr class="table-warning"><td colspan="5" class="black">SELL '.$symbol.' - insufficient funds to meet minimum ('.$minNotional.' '.$quote.') trade requirement.</td></tr>';
                        //                     }
                        //                 } 
                        //             }
                        //         }
                        //     }
                        // }
                    }

                    // convert from quote value into number of coins
                    $minQty = $tradeableAssets[$symbol]['minQty'];
                    // $lot_qty = floorp($lot_qty, $minQty);
                    $lot_qty = number_format($lot_qty / $price, $minQty, ".", "");

                    if($lot_qty != 0 && $buynow != 0 && $noFunds != 100){
                        $api->useServerTime();
                        // $order = $api->buyTest($symbol, $lot_qty, 0, "MARKET");
                        $msg .= '<tr class="table-warning"><td colspan="5" class="black">BUY '.$symbol.' - attempting to BUY ('.$lot_qty.' at: '.$price.') per coin.</td></tr>';
                        $order = $api->buy($symbol, $lot_qty, 0, "MARKET");
                        $price = 0;
                        $qtyQuote = 0;
                        
                        if(isset($order['cummulativeQuoteQty'])){
                            $qtyQuote = $order['cummulativeQuoteQty'];
                            $qty = $order['executedQty'];
                            $price = number_format(($qtyQuote / $qty), 8, ".", "");
                            if(isset($balances[$sym2]["coin_qty"])){
                                $balances[$sym2]["coin_qty"] += $qty;
                            } else {
                                $balances[$sym2]["coin_qty"] = $qty;
                            }
                            $balances[$quote]["coin_qty"] -= $qtyQuote;
                            $core_account_value -= $qtyQuote;
                        }
                        
                        // send back <th>ACTION</th><th>Trade Pair</th><th>Lot Qty</th><th>BTC Price</th><th>QTY</th>
                        // echo '<br>'.$order.'<br>';
                        $newtrades[] = $order;
                        if($quote == 'BUSD' || $quote == 'USDT'){
                            $price = '&dollar;'.number_format($price, 4, ".", "");
                            $qtyQuote = number_format($qtyQuote, 1, ".", "");
                        }
                        $msg .= '<tr><td class="table-success black">BUY</td><td>'.$symbol.'</td><td>'.$lot_qty.'</td><td>'.$price.'</td><td>'.$qtyQuote.'</td></tr>';
                    }

                }
    
        } else if(isset($balances[$sym2])){

            // FIND QUANTITIES IN BITCOIN
            $lots_quote = $quantity * $lot_size;
            $lot_qty = $lots_quote / $balances[$sym2]["btc_price"];
            
            if($lot_qty > $balances[$sym2]["coin_qty"]){
                $lots_quote = $balances[$sym2]["btc_value"];
                $lot_qty = $balances[$sym2]["coin_qty"];
            }

            if($quantity == 20){
                $lots_quote = $balances[$sym2]["btc_value"];
                $lot_qty = $balances[$sym2]["coin_qty"];
            }


            // IF QUOTE IS NOT BITCOIN FIND VALUE IN QUOTE PRICE
            // if($quote == 'USDT' || $quote == 'BUSD'){
            //     $lots_quote = $lots_quote * $balances['BTC']["btc_price"];
            // }if($quote == 'ETH'){
            //     $lots_quote = $lots_quote / $balances['ETH']["btc_price"];
            // }


            $minNotional = $tradeableAssets[$symbol]['minNotional'];
            if($lots_quote < $minNotional){
                $noFunds = 100;
                $msg .= '<tr class="table-warning"><td colspan="5" class="black">SELL '.$symbol.' - insufficient funds to meet minimum ('.$minNotional.' '.$quote.') trade requirement.</td></tr>';
            }

            $minQty = $tradeableAssets[$symbol]['minQty'];

            $lot_qty = floorp($lot_qty, $minQty);
            // if($minQty == 0){ $lot_qty--; }

            if($lot_qty > 0 && $noFunds != 100 && $lot_qty <= $balances[$sym2]["coin_qty"]){
                $api->useServerTime();
                
                // public function buy(string $symbol, $quantity, $price, string $type = "LIMIT", array $flags = [])
                // $order = $api->sellTest($symbol, $lot_qty, 0, "MARKET");
                $msg .= '<tr class="table-warning"><td colspan="5" class="black">SELL '.$symbol.' - attempting to SELL ('.$lot_qty.' at: '.$price.') per coin. uses weird floorp</td></tr>';
                $order = $api->sell($symbol, $lot_qty, 0, "MARKET");
                $soldprice = 0;
                $qtyQuote = 0;
                
                if(isset($order['cummulativeQuoteQty'])){
                    $qtyQuote = $order['cummulativeQuoteQty'];
                    $qty = $order['executedQty'];
                    $soldprice = number_format(($qtyQuote / $qty), 8, ".", "");
                    $balances[$sym2]["coin_qty"] -= $qty;
                    $balances[$quote]["coin_qty"] += $qtyQuote;
                    $core_account_value += $qtyQuote;
                }
                
                // send back <th>ACTION</th><th>Trade Pair</th><th>Lot Qty</th><th>BTC Price</th><th>QTY</th>
                // echo '<br>'.$order.'<br>';
                // $lot_qty = number_format($lot_qty, 8, ".", "");

                // if($lot_qty != 0 && $balance[3] >= $lot_qty && $noFunds != 100){
                //     $api->useServerTime();
                //     // $order = $api->sellTest($symbol, $lot_qty, 0, "MARKET");
                //     $order = $api->sell($symbol, $lot_qty, 0, "MARKET");

                //     $balances[$key][1] -= $lot_qty;
                //     $memo .= "$symbol SELL $lot_qty $sym2 = $quantity Lots<br/>\n\n";
                //     // echo '<br>'.$order.'<br>';
                //     $newtrades[] = $order;
                // }  

                $newtrades[] = $order;
                if($quote == 'BUSD' || $quote == 'USDT'){
                    $soldprice = '&dollar;'.number_format($soldprice, 4, ".", "");
                    $qtyQuote = number_format($qtyQuote, 1, ".", "");
                }
                $msg .= '<tr><td class="table-danger black">SELL</td><td>'.$symbol.'</td><td>'.$qty.'</td><td>'.$soldprice.'</td><td>'.$qtyQuote.'</td></tr>';
            }               
    
        }
    } catch (\Throwable $th) {
        $memo .= "$th - something went wrong! - Attempted a $action order on $symbol pair for coin $sym2<br/><br/>";
    }

    return $msg;
}