<?php
require 'config.php';
require EXCHANGE.'/bnb-api.php';
require 'FUNK/functions.php';

$hour = date("jS M Y - H:i");
$tradeableAssets = [];
$bluechips = array('ZRX','AAVE','ARDR','REP','BAND','BAT','BNB','BCH','BTG','BUSD','ADA','LINK','ATOM','DASH','MANA','DCR','EGLD','ENJ','EOS','ETH','ETC','FIRO','GLM','GXS','ICX','IOTA','KMD','KNC','LSK','LTC','LRC','XMR','NANO','XEM','NEO','OMG','ONT','PIVX','QTUM','REN','XRP','SNT','XLM','SNX','USDT','XTZ','THETA','TRX','WAVES','ZEN','ZIL');
$balances = [];
$newtrades = [];
$core_account_value = 0;
$core_cur = 'BTC';
$bot_mode = 'UNKNOWN';
$memo = "";
$coin_count = 50;
$count = 0;

if(EXCHANGE == 'KNC'){
    $coins_arr = get_json_contents('https://api.binance.com/api/v3/ticker/24hr');
    $exchange_info = get_json_contents('https://api.binance.com/api/v3/exchangeInfo');
} else {
    $coins_arr = get_json_contents('https://api.binance.com/api/v3/ticker/24hr');
    $exchange_info = get_json_contents('https://api.binance.com/api/v3/exchangeInfo');
}

//================================================================
// GET LIST OF ALL VALID TRADEABLE ASSETS
//================================================================
foreach ($exchange_info["symbols"] as $symbol) {
    $count++;
    if($symbol["isSpotTradingAllowed"]==true && $symbol['status'] == "TRADING"){

        if($symbol['quoteAsset']=='BTC' || $symbol['quoteAsset']=='USDT' || $symbol['quoteAsset']=='BUSD' || $symbol['quoteAsset']=='ETH'){

            if(!isset($tradeableAssets[$symbol['baseAsset']])){

                foreach ($symbol['filters'] as $filter) {

                    if($filter['filterType'] == 'MIN_NOTIONAL'){
                        $minNotional = $filter['minNotional'];
                    }
                    if($filter['filterType'] == 'LOT_SIZE'){
                        switch ($filter['minQty']) {
                            case 0.00000100:
                                $minQty = 6;
                                break;
                            case 0.00001000:
                                $minQty = 5;
                                break;
                            case 0.00010000:
                                $minQty = 4;
                                break;
                            case 0.00100000:
                                $minQty = 3;
                                break;
                            case 0.01000000:
                                $minQty = 2;
                                break;
                            case 0.10000000:
                                $minQty = 1;
                                break;                
                            default:
                                $minQty = 0;
                                break;
                        }
                    }
                }
                $tradeableAssets[$symbol['symbol']] = Array("base"=>$symbol['baseAsset'], "quote"=>$symbol['quoteAsset'], "minNotional"=>$minNotional, "minQty"=>$minQty);
            }
        }
    }
}
//================================================================







//================================================================
// GET NEW TRADES
//================================================================
sleep(rand(1,50));
$trades = get_json_contents('http://api.moocharoo.ninja/v1/strategies/'.TRACKING_CHIP);
$buytrades = [];
$selltrades = [];

if(isset($trades['data']['coins'])){
    $coin_count = $trades['data']['coins'];
}
if(isset($trades['data']['core_cur'])){
    $core_cur = $trades['data']['core_cur'];
} 
if(isset($trades['data']['bot_mode'])){
    $bot_mode = $trades['data']['bot_mode'];
}
if(isset($trades['data']['tickers'])){
    $bluechips = $trades['data']['tickers'];
}

switch ($core_cur) {
    case 'BTC':
        $precision = 8;
        break;
    case 'ETH':
        $precision = 8;
        break;
    default:
        $precision = 2;
        break;
}

echo '<html lang="en"><head><title>KENNEL</title><link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
<style>body{font-family: "Courier New", monospace; float:left; background-color:#2b2c2f; color:chartreuse; width: 100%;} h3{color:ghostwhite; font-style: oblique;} td{color:aliceblue;} td.black{color:black;} span{color:hotpink; font-weight: 600;} .kennel{margin: 80 auto; width: fit-content; border: 3px solid #35373b; border-style: dotted; padding: 50px;} </style></head><body>';
echo '<div class="container"><div class="row"><h3>Execution at '.$hour.'<br><small>STRATEGY CORE CURRENCY: '.$core_cur.'</small></h3><h6>BOT MODE: '.$bot_mode.'</h6><div class="col">';
//================================================================







//================================================================
// GET LIST OF BALANCES
//================================================================
try {
    $api = new Binance\API(API_KEY, API_SECRET);
    $api->caOverride = true;
    $api->useServerTime();
    // Get all of your positions, including estimated BTC value
    $balancesALL = $api->balances();
    
    foreach ($balancesALL as $key => $balance) {
        if($balance['available'] > 0){

            $ticker = "";
            $coin_qty = $balance['available'];
            $ticker = $key.$core_cur;

            if($ticker == 'BTCBTC' || $ticker == 'USDTBTC' || $ticker == 'USDTUSDT'){
                $ticker = 'BTCUSDT';
            }

            foreach ($coins_arr as $coin) {
                if($coin['symbol'] == $ticker){
                    $coin_price = $coin['lastPrice'];
                }
            }
            if($core_cur != $key){
                $btc_value = number_format(($coin_qty * $coin_price), $precision, '.', '');
            } else {
                $btc_value = number_format($coin_qty, $precision, '.', '');
            }

            if($key == $core_cur){
                $core_account_value += $btc_value;
                $balances[$key] = Array("lots"=>0, "coin_qty"=>$coin_qty, "btc_price"=>$coin_price, "btc_value"=>$btc_value);
            } else {
                foreach ($bluechips as $bluechip) {
                    if($key==$bluechip){
    
                        $core_account_value += $btc_value;
                        
                        $balances[$key] = Array("lots"=>0, "coin_qty"=>$coin_qty, "btc_price"=>$coin_price, "btc_value"=>$btc_value);
    
                    }
                }
            }
        }
    }
    $max_lots = $coin_count * 20;
    if($core_account_value > 0){
        $lot_size = number_format(($core_account_value / ($max_lots)), 8, '.', '');
    } else { $lot_size = 0; }
    $full_lots = number_format(($lot_size * 20), 8, '.', '');
    $core_account_value = number_format($core_account_value, 8, '.', '');
    
    if(count($balances) <= 1){
        $balances[$core_cur]["lots"] = 1000;
    } else {
        foreach ($balances as $key => $balance) {
            $balances[$key]["lots"] = floor($balance["btc_value"] / $lot_size);  
            
            //remove from array if Lots are less than 1
            if($balances[$key]["lots"] < 1){
                unset($balances[$key]);
            }
        }  
    } 
    ksort($balances);
    

    echo '<h3><u>MCN Balances excluding coins ON ORDER</u></h3><table class="table table-striped"><thead class="table-dark"><th>COIN</th><th>QTY</th><th>'.$core_cur.' Value</th><th>LOTS</th></thead><tbody><tr><td>'.$core_cur.'</td><td>'.$balances[$core_cur]["coin_qty"].'</td><td>'.$balances[$core_cur]["btc_value"].'</td><td>'.$balances[$core_cur]["lots"].'</td></tr>';
    foreach($balances as $coin => $balance){
        if($coin != $core_cur){
            echo '<tr><td>'.$coin.'</td><td>'.$balance["coin_qty"].'</td><td>'.$balance["btc_value"].'</td><td>'.$balance["lots"].'</td></tr>';
        }
    }  
    if($core_cur == 'USDT' || $core_cur == 'BUSD'){
        echo '</tbody></table><p>'.$core_cur.' and MCN Coins Total Value in '.$core_cur.': <span>$'.$core_account_value.'</span></p><p>Lot size in '.$core_cur.': <span>$'.$lot_size.'</span></p><p>Coins Available to Trade: <span>'.$coin_count.'</span></p><p>Trading Account Max Lots: <span>'.$max_lots.'</span></p>';
    } else {
        echo '</tbody></table><p>'.$core_cur.' and MCN Coins Total Value in '.$core_cur.': <span>'.$core_account_value.'</span></p><p>Lot size in '.$core_cur.': <span>'.$lot_size.'</span></p><p>Coins Available to Trade: <span>'.$coin_count.'</span></p><p>Trading Account Max Lots: <span>'.$max_lots.'</span></p>';
    }
    
    

} catch (\Throwable $th) {
    echo '<div class="alert alert-danger" role="alert">'.$th.' - something went wrong! -- Tried to connect to API and obtain balances from exchange</div>';
}
echo '</div><div class="col">';
//================================================================







//================================================================
// GET LIST OF NEW TRADES
//================================================================
echo "<h3><u>New MCN Trade Signals Received</u></h3><ol>";
if(isset($trades['data']['trades'])){
    
    foreach ($trades['data']['trades'] as $key => $trade) {

        if($trade['coin']==null){
            $ticker = $trade['ticker'];
        } else {
            $ticker = $trade['coin'].$core_cur;
        }

        if(isset($tradeableAssets[$ticker])){
        
            if($trade['win_lose'] == ''){
                $buysell = 'B';
                $current_price = $trade['open_price'];

                if(isset($buytrades[$ticker])){
                    $buytrades[$ticker]["lots"]++;
                } else {
                    $buytrades[$ticker] = Array("buysell"=>$buysell, "current_price"=>$current_price, "lots"=>1, "base"=>$tradeableAssets[$ticker]["base"], "quote"=>$tradeableAssets[$ticker]["quote"]);
                }
            } else {
                $buysell = 'S';
                $current_price = $trade['close_price'];

                if(isset($selltrades[$ticker])){
                    $selltrades[$ticker]["lots"]++;
                } else {
                    $selltrades[$ticker] = Array("buysell"=>$buysell, "current_price"=>$current_price, "lots"=>1, "base"=>$tradeableAssets[$ticker]["base"], "quote"=>$tradeableAssets[$ticker]["quote"]);
                }
            }
        }
    }

    foreach ($selltrades as $coin => $trade) {
        echo '<li><span>SELL - '.$trade["lots"].' Lots of '.$coin.'</span></li>';
    }
    foreach ($buytrades as $coin => $trade) {
        echo '<li><b>BUY - '.$trade["lots"].' Lots of '.$coin.'</b></li>';
    }

} else {
    echo '<li><b>No trade signals at this time...</b></li></ol>';
}
//================================================================







//================================================================
// EXECUTE NEW SIGNALS
//================================================================
echo '<h3 style="margin-top: 50px;"><u>Executed Trades</u></h3><table class="table table-striped"><thead class="table-dark"><th>ACTION</th><th>Trade Pair</th><th>Lot Qty</th><th>'.$core_cur.' Price</th><th>QTY</th></thead>';

foreach ($selltrades as $coin => $trade) {
    // $selltrades[$ticker] = Array($buysell, $ticker, $current_price, 1, $sym);
    $order = addMCNSignal($trade["buysell"], $coin, $trade["current_price"], $trade["lots"], $trade["base"], $trade["quote"]);
    echo $order;
}
foreach ($buytrades as $coin => $trade) {
    $order = addMCNSignal($trade["buysell"], $coin, $trade["current_price"], $trade["lots"], $trade["base"], $trade["quote"]);
    echo $order;
}
if(!isset($order)){
    echo '<tr class="table-secondary"><td colspan="5" class="black">-- No new trade signals to execute --</td></tr>';
}
echo '</table>';
//================================================================
                    



                
  

//================================================================
// GET NEW LIST OF BALANCES
//================================================================
sleep(rand(1,50));
try {
    $balances = [];
    $api->useServerTime();
    // Get all of your positions, including estimated BTC value
    $balancesALL = $api->balances();

    foreach ($balancesALL as $key => $balance) {
        if($balance['available'] != 0){

            $ticker = "";
            $coin_qty = $balance['available'];

            if($key == 'BTC'){
                $btc_price = 1;
                $ticker = 'BTC';
            } else {

                $ticker = $key.'BTC';
                if($ticker == 'USDTBTC'){
                    $ticker = 'BTCUSDT';
                }
                foreach ($coins_arr as $coin) {
                    if($coin['symbol'] == $ticker){
                        $btc_price = $coin['lastPrice'];
                    }
                }

            }


            // CANCEL ANY UNFILLED OPEN ORDERS
            // if($balance['onOrder'] != 0){
            //     $api->cancelOpenOrders($ticker);
            // }

            if($key == 'USDT'){
                $btc_value = number_format(($coin_qty / $btc_price), 8, '.', '');
            } else {
                $btc_value = number_format(($btc_price * $coin_qty), 8, '.', '');
            }
            $core_account_value += $btc_value;

            // example: EOS = EOSBTC, 205, 0.00007730, 0.01569568
            $balances[$key] = Array($ticker, $coin_qty, $btc_price, $btc_value);

        }
    }
} catch (\Throwable $th) {
    $memo .= "$th - something went wrong! - ERR9 - Error in getting all of your positions, including estimated BTC value<br/> \n\n";
}
//================================================================





//================================================================
// RECENT TRADE DATA BACK TO MCN FOR ACCOUNTING
//================================================================
$feedback = Array('chip' => TRACKING_CHIP, 'balances' => $balances, 'trades' => $newtrades);
function callAPI($url, $data){
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_POST, 1);
    curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
    // OPTIONS:
    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_HTTPHEADER, array(
       'Content-Type: application/json',
    ));
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    // EXECUTE:
    $result = curl_exec($curl);
    if(!$result){die("Connection Failure");}
    curl_close($curl);
    // echo $result;
    return $result;
 }
 try {
    $make_call = callAPI('http://api.moocharoo.ninja/v1/newtrades', json_encode($feedback));
    $response = json_decode($make_call, true);
 } catch (\Throwable $th) {
     //throw $th;
 }
 //================================================================


 // display errors:
echo $memo;
echo "</div></div><hr></div></body></html>";

?>