**KENNEL VERSION: 1.0.5**

## GETTING STARTED

To use the Moocharoo Ninja decentralised trading bot you will need to have upgraded your account to the level of MASTER NINJA.

*We have put together a video setup guide for both Mac and Windows, [you can see that video here](https://youtu.be/-7GJf69XdVI).*

WE STRONGLY RECOMMEND that you follow the written docs EXACTLY step by step during set up to avoid unnecessary errors.

**Download** the docs from the 'SETTINGS' tab on the Bot Shop.
---

## UPDATE YOUR BOT

There are many ways that you can update your bot but below is the list of steps that we have found to be the easiest for members to understand and carryout.

**Long Method:**

1. ssh back into your server as per the docs.
2. **cd ../var/www/html/cryptoshi**
3. Enter the config file and copy and paste the contents somewhere safe for 5 mins, type - **nano config.php**
4. **Ctrl+X** to exit, then hit **Y** and **ENTER**
5. now go out of the cryptoshi folder with: **cd ../**
6. and remove the bot: **rm -r cryptoshi**
7. now clone the latest version: **git clone https://cryptoshibot@bitbucket.org/cryptoshibot/cryptoshi.git**
8. **cd cryptoshi**
9. Go into the config file and paste in your API details that you copied earlier: **nano config.php**
10. **Ctrl+X** to leave when finished, hit **Y** and then **ENTER**

**Short Method:**

1. ssh back into your server as per the docs.
2. **cd ../var/www/html/cryptoshi**
3. **git pull**


Lastly: **Ctrl+D**, to detach from the server 👍

---