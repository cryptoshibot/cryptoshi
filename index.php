<?php
if(isset($_POST['newbot'])){
    $trackingchip = trim($_POST['trackingchip']);
    $exchangeoption = trim($_POST['exchangeoption']);
    $apikey = trim($_POST['apikey']);
    $apisecret = trim($_POST['apisecret']);

    $newconfig = "<?php
    define('TRACKING_CHIP', '$trackingchip');
    define('API_KEY', '$apikey');
    define('API_SECRET', '$apisecret');
    define('EXCHANGE', '$exchangeoption');";
    $f = fopen("config.php", "w");
    fwrite($f, $newconfig);
    fclose($f);
    sleep(5);
}

require 'config.php';

if(!defined('EXCHANGE')){
    echo '<html lang="en"><head><title>KENNEL</title><link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
<style>body{font-family: "Courier New", monospace; float:left; background-color:#2b2c2f; color:chartreuse; width: 100%;} h3{color:ghostwhite; font-style: oblique;} td{color:aliceblue;} td.black{color:black;} span{color:hotpink; font-weight: 600;} .kennel{margin: 80 auto; width: fit-content; border: 3px solid #35373b; border-style: dotted; padding: 50px;} </style></head><body>';

    echo '<div class="container"></div><div class="kennel">
<form action="'.$_SERVER['PHP_SELF'].'" method="post">
    <h3>KENNEL BUILD COMPONENTS</h3>
  <div class="mb-3">
    <label for="trackingchip" class="form-label">Mutt Tracking Chip:</label>
    <input type="text" class="form-control" id="trackingchip" name="trackingchip" aria-describedby="mutt tracking chip">
    <div class="form-text">you will find your Mutts Tracking Chip under <a href="https://moocharoo.ninja/trading-bot.html" target="_blank">SETTINGS</a></div>
  </div>
  <div class="mb-3">
    <label for="exchangeoption" class="form-label">Exchanges:</label>
      <select class="form-select" id="exchangeoption" name="exchangeoption" aria-label="Default select example">
        <option value="BNB" selected>Binance</option>
        <option value="BUS">Binance US</option>
        <option value="KCN" disabled>Kucoin</option>
      </select>
  </div>
  <div class="mb-3">
    <label for="apikey" class="form-label">Exchange API Key:</label>
    <input type="text" class="form-control" id="apikey" name="apikey">
  </div>
  <div class="mb-3">
    <label for="apisecret" class="form-label">Exchange API Secret:</label>
    <input type="text" class="form-control" id="apisecret" name="apisecret">
  </div>
  <button style="margin-top: 25px;" type="submit" name="newbot" class="btn btn-light">Submit</button>
</form>
</div></div>
</body></html>';
} else {
    echo "<html>
    <head><title>KENNEL</title><link href='https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css' rel='stylesheet' integrity='sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x' crossorigin='anonymous'>
    <style>body{font-family: 'Courier New', monospace; float:left; background-color:#2b2c2f; color:chartreuse; width: 100%;} span{color:deeppink;} .kennel{margin: 80 auto; width: fit-content; border: 3px solid #35373b; border-style: dotted; padding: 50px;} </style></head>
    <body>
		<div class='container'></div><div class='kennel'><p><br/>
&nbsp;&nbsp;&nbsp;.-------------.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;.&nbsp;&nbsp;&nbsp;&nbsp;.&nbsp;&nbsp;&nbsp;*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br/>
&nbsp;&nbsp;/_/_/_/_/_/_/_/&nbsp;\&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;.&nbsp;&nbsp;&nbsp;)&nbsp;&nbsp;&nbsp;&nbsp;.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br/>
&nbsp;//_/_/_/_/_/_//&nbsp;_&nbsp;\&nbsp;__&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br/>
/_/_/_/_/_/_/_/|/&nbsp;\.'&nbsp;.`-o&nbsp;&nbsp;&nbsp;.-.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;.-.&nbsp;&nbsp;&nbsp;<br/>
&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;||-'(/&nbsp;,--'&nbsp;&nbsp;(&nbsp;&nbsp;&nbsp;`-._________.-'&nbsp;&nbsp;&nbsp;)&nbsp;&nbsp;<br/>
&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;||&nbsp;&nbsp;_&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;>=&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span>v:1.0.5</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=<&nbsp;&nbsp;&nbsp;<br/>
&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;||''&nbsp;||&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(&nbsp;&nbsp;&nbsp;,-'`&macr;&macr;&macr;&macr;&macr;&macr;&macr;`'-,&nbsp;&nbsp;&nbsp;)&nbsp;&nbsp;<br/>
&nbsp;|_____________||&nbsp;|_|L&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;`-'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;`-'&nbsp;&nbsp;&nbsp;<br/>
 </p></div><div class='text-center'><h2><a style='margin-top: 70px;' href='trades.html' class='btn btn-light'>Recent Trades</a></h2></div></div>
<!--<h1>DOG<br>-->
</h1>
</body>
</html>";
}